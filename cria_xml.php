<?php
#versao do encoding xml
$dom = new DOMDocument("1.0", "ISO-8859-1");

#retirar os espacos em branco
$dom->preserveWhiteSpace = false;

#gerar o codigo
$dom->formatOutput = true;

#criando o nó principal (root)
$root = $dom->createElement("lista");

#nó filho (user)
$user = $dom->createElement("user");

#setanto nomes e atributos dos elementos xml (nós)
$nome = $dom->createElement("nome", trim($_POST['nome']));
$telefone = $dom->createElement("usuario", trim($_POST['usuario']));
$endereco = $dom->createElement("senha", trim(md5($_POST['senha'])));

#adiciona os nós (informacaoes do user) em user
$user->appendChild($nome);
$user->appendChild($telefone);
$user->appendChild($endereco);

#adiciona o nó user em (root) agenda
$root->appendChild($user);
$dom->appendChild($root);

# Para salva o arquivo
$dom->save("users.xml");

#cabeçalho da página
header("Content-Type: text/xml");
# imprime o xml na tela
print $dom->saveXML();
?>